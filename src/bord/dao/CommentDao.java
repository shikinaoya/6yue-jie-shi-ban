package bord.dao;

import static bord.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bord.beans.Comment;
import bord.beans.User;
import bord.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("message_id");
            sql.append(", name");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", login_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // message_id
            sql.append(", ?"); // name
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // create_date
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getMessageId());
            ps.setString(2, user.getName());
            ps.setString(3, comment.getText());
            ps.setString(4, user.getLoginId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public List<Comment> getComments(Connection connection) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM Comments ORDER BY created_date DESC");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                int messageId = rs.getInt("message_id");
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String loginId = rs.getString("login_id");

                Comment comment = new Comment();
                comment.setMessageId(messageId);
                comment.setId(id);
                comment.setName(name);
                comment.setText(text);
                comment.setCreatedDate(createdDate);
                comment.setLoginId(loginId);


                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments WHERE id = " + id);

            ps = connection.prepareStatement(sql.toString());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}