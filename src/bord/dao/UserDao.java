package bord.dao;

import static bord.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bord.beans.User;
import bord.exception.NoRowsUpdatedRuntimeException;
import bord.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", office_id");
            sql.append(", position_id");
            sql.append(", status");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // office_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // status
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getOfficeId());
            ps.setInt(5, user.getPositionId());
            ps.setInt(6, user.getStatus());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int officeId = rs.getInt("office_id");
                int positionId = rs.getInt("position_id");
                int status = rs.getInt("status");
                int id = rs.getInt("id");

                User user = new User();
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setOfficeId(officeId);
                user.setPositionId(positionId);
                user.setStatus(status);
                user.setId(id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if(userList.isEmpty() == true){
                return null;
            } else{
            	return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT users.login_id");
            sql.append(", users.id");
            sql.append(", users.password");
            sql.append(", users.name");
            sql.append(", users.status");
            sql.append(", users.office_id");
            sql.append(", users.position_id");
            sql.append(", offices.office");
            sql.append(", positions.position");
            sql.append(" FROM( users INNER JOIN offices ON users.office_id = offices.office_id)");
            sql.append(" INNER JOIN positions ON users.position_id = positions.position_id");
            ps = connection.prepareStatement(sql.toString());

            ResultSet secondrs = ps.executeQuery();
            List<User> userJoinList = toUserJoinList(secondrs);
            if (userJoinList.isEmpty() == true) {
                return null;
            } else {
                return userJoinList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserJoinList(ResultSet secondrs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (secondrs.next()) {
                String loginId = secondrs.getString("login_id");
                String password = secondrs.getString("password");
                String name = secondrs.getString("name");
                int officeId = secondrs.getInt("office_id");
                int positionId = secondrs.getInt("position_id");
                int status = secondrs.getInt("status");
                String office = secondrs.getString("office");
                String position = secondrs.getString("position");
                int id = secondrs.getInt("id");

                User user = new User();
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setOfficeId(officeId);
                user.setPositionId(positionId);
                user.setStatus(status);
                user.setOffice(office);
                user.setPosition(position);
                user.setId(id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(secondrs);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
        	if(user.getPassword().isEmpty()){
        		StringBuilder sql = new StringBuilder();
                sql.append("UPDATE users SET");
                sql.append("  login_id = ?");
                sql.append(", name = ?");
                sql.append(", office_id = ?");
                sql.append(", position_id = ?");
                sql.append(", status = ?");
                sql.append(" WHERE");
                sql.append(" id = ?");

                ps = connection.prepareStatement(sql.toString());

                ps.setString(1, user.getLoginId());
                ps.setString(2, user.getName());
                ps.setInt(3, user.getOfficeId());
                ps.setInt(4, user.getPositionId());
                ps.setInt(5, user.getStatus());
                ps.setInt(6, user.getId());

                int count = ps.executeUpdate();
                if (count == 0) {
                    throw new NoRowsUpdatedRuntimeException();
                }
        	}else{
        		StringBuilder sql = new StringBuilder();
                sql.append("UPDATE users SET");
                sql.append("  login_id = ?");
                sql.append(", password = ?");
                sql.append(", name = ?");
                sql.append(", office_id = ?");
                sql.append(", position_id = ?");
                sql.append(", status = ?");
                sql.append(" WHERE");
                sql.append(" id = ?");

                ps = connection.prepareStatement(sql.toString());

                ps.setString(1, user.getLoginId());
                ps.setString(2, user.getPassword());
                ps.setString(3, user.getName());
                ps.setInt(4, user.getOfficeId());
                ps.setInt(5, user.getPositionId());
                ps.setInt(6, user.getStatus());
                ps.setInt(7, user.getId());

                int count = ps.executeUpdate();
                if (count == 0) {
                    throw new NoRowsUpdatedRuntimeException();
                }
        	}
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}