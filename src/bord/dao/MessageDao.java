package bord.dao;

import static bord.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bord.beans.Message;
import bord.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", name");
            sql.append(", created_date");
            sql.append(", login_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); //name
            sql.append(", CURRENT_TIMESTAMP"); // create_date
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getSubject());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setString(4, message.getName());
            ps.setString(5, message.getLoginId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public List<Message> getMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM messages ORDER BY created_date DESC limit " + num);
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Message> ret = toMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Message> toMessageList(ResultSet rs)
            throws SQLException {

        List<Message> ret = new ArrayList<Message>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String name = rs.getString("name");
                String loginId = rs.getString("login_id");

                Message message = new Message();
                message.setId(id);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreatedDate(createdDate);
                message.setName(name);
                message.setLoginId(loginId);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE id = " + id);

            ps = connection.prepareStatement(sql.toString());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Message> findOfCategory(Connection connection, String category) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM messages WHERE category LIKE '%"+ category +"%'");
            sql.append(" ORDER BY created_date DESC");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Message> ret = toMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Message> findOfDate(Connection connection, String startDate, String endDate) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM messages WHERE created_date < '" + startDate + " 23:59:59");
            sql.append("' AND created_date >= '" + endDate + " 00:00:00' ORDER BY created_date DESC");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Message> ret = toMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Message> findOfDateAndCategory(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM messages WHERE category LIKE '%"+message.getCategory()+"%' AND created_date < '"+message.getCreatedDateEnd()+" 23:59:59");
            sql.append("' AND created_date >= '"+message.getCreatedDateStart()+" 00:00:00' ORDER BY created_date DESC");
            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Message> ret = toMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}