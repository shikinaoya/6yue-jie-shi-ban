package bord.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import bord.beans.User;

@WebFilter(urlPatterns = {"/management","/editUser","/signup"})
public class AdminFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest) request).getSession();

        if (user.getOfficeId() == 1 && user.getPositionId() == 1) {
        	chain.doFilter(request, response);
        }else{
        	messages.add("管理画面へのアクセス権限がありません");
        	session.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("home").forward(request, response);
        }
    }

	@Override
	public void destroy() {

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}
}
