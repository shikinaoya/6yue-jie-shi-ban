package bord.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bord.beans.Comment;
import bord.beans.Message;
import bord.beans.User;
import bord.service.CommentService;
import bord.service.MessageService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");

        if (user != null) {
            List<Message> messages = new MessageService().getMessage();
            request.setAttribute("messages", messages);

            List<Comment> comment = new CommentService().getComment();
            request.setAttribute("comments", comment);

            request.getRequestDispatcher("home.jsp").forward(request, response);
        }else{
            response.sendRedirect("login");
        }
    }
}