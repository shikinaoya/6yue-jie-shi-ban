package bord.controller;

import static bord.utils.DBUtil.*;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bord.beans.User;
import bord.dao.UserDao;
import bord.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        User user = new User();
        user.setLoginId(request.getParameter("loginId"));
        user.setName(request.getParameter("name"));
        user.setPassword(request.getParameter("password"));
        user.setOfficeId(Integer.parseInt(request.getParameter("officeId")));
        user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
        user.setStatus(Integer.parseInt(request.getParameter("status")));


        if (isValid(request, messages) == true) {

            new UserService().register(user);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String passwordCheck = request.getParameter("passwordCheck");
        Connection connection = null;
        connection = getConnection();
        UserDao userDao = new UserDao();

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("IDを入力してください");
        } else{
        	if (loginId.matches("^[0-9a-zA-Z]+") != true) {
                messages.add("IDは半角英数字のみで入力してください");
            }
        	if (loginId.matches(".{6,20}") != true) {
                messages.add("IDは6文字以上20文字以下にしてください");
            }
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else{
//        	if (password.matches("[ -~]+") != true) {
//                messages.add("パスワードは半角英数字および記号のみで入力してください");
//        	}
        	if (password.matches("[!-~]{6,20}") != true) {
                messages.add("パスワードは6文字以上20文字以下にしてください");
            }else{
            	if (!password.equals(passwordCheck)) {
                    messages.add("パスワードが一致しません");
                }
            }
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください");
        } else{
        	if (10 < name.length()) {
                messages.add("名称は10文字以下にしてください");
            }
        }
        if(userDao.getUser(connection, loginId) != null){
        	messages.add("使用されているIDです");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}