package bord.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bord.beans.Message;
import bord.beans.User;
import bord.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("newMessage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setText(request.getParameter("text"));
            message.setSubject(request.getParameter("subject"));
            message.setCategory(request.getParameter("category"));
            message.setName(user.getName());
            message.setLoginId(user.getLoginId());

            new MessageService().register(message);

            response.sendRedirect("home");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newMessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
    	String category = request.getParameter("category");
        String message = request.getParameter("text");

        if (StringUtils.isEmpty(subject) == true) {
            messages.add("件名を入力してください");
        } else{
        	if (30 < subject.length()) {
                messages.add("件名は30文字以下にしてください");
            }
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        } else{
        	if (10 < category.length()) {
                messages.add("カテゴリーは10文字以下にしてください");
            }
        }
        if (StringUtils.isEmpty(message) == true) {
            messages.add("本文を入力してください");
        } else{
        	if (1000 < message.length()) {
                messages.add("本文は1000文字以下にしてください");
            }
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}