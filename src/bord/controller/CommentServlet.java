package bord.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bord.beans.Comment;
import bord.beans.User;
import bord.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

        	User user = (User) request.getSession().getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
            comment.setText(request.getParameter("text"));

            new CommentService().register(comment,user);

            response.sendRedirect("home");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("home");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("text");

        if (StringUtils.isEmpty(message) == true) {
            messages.add("コメントを入力してください");
        } else{
        	if (500 < message.length()) {
                messages.add("コメントは500文字以下にしてください");
            }
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}