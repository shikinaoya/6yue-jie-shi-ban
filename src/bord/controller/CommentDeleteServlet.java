package bord.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bord.service.CommentService;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	new CommentService().delete(Integer.parseInt(request.getParameter("id")));
    	response.sendRedirect("home");
    }
}