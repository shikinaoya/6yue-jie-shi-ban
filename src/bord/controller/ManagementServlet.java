package bord.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bord.beans.User;
import bord.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");

        if (user != null) {
        	List<User> users = new UserService().getUserList();
            request.setAttribute("users", users);

            request.getRequestDispatcher("management.jsp").forward(request, response);
        }else{
            response.sendRedirect("login.jsp");
        }
    }
}