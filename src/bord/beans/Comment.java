package bord.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int message_id;
    private int id;
    private String name;
    private String text;
    private Date createdDate;
    private String login_id;

    public int getMessageId() {
        return message_id;
    }
    public void setMessageId(int message_id) {
        this.message_id = message_id;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLoginId() {
        return login_id;
    }
    public void setLoginId(String login_id) {
        this.login_id = login_id;
    }
}