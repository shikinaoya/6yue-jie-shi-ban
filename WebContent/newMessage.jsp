<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- jQuery読み込み -->
		<script type="text/javascript" src="./js/jquery.min.js"></script>

		<!-- BootstrapのCSS読み込み -->
		<link rel="stylesheet" href="./css/bootstrap.min.css" rel="stylesheet">
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<!-- BootstrapのJS読み込み -->
		<script src="./js/bootstrap.min.js"></script>

		<!-- AngularJS読み込み -->
		<script src="./js/angular.min.js"></script>

		<!-- JSファイル読み込み -->
		<script src="./js/showlength.js"></script>

        <title>新規投稿</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="bg-light">
                    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
           <div class="header">
		        <ol class="breadcrumb">
			        <li class="active"><a href="home" class="btn btn-link">ホーム</a></li>
			        <li><a href="newMessage" class="btn btn-link">新規投稿</a></li>
			        <li><a href="management" class="btn btn-link">ユーザー管理</a></li>
			        <li><a href="logout" class="btn btn-link">ログアウト</a></li>
			   </ol>
    		</div>


<div class="form-area">
        <form action="newMessage" method="post">

         	<label for="subject">件名</label>
            <input name="subject" class="form-control" ng-model="subject" value="${subject}"/>
			<p>{{"残り" + (30 - subject.length) + "文字"}}</p>
            <label for="category">カテゴリー</label>
            <input name="category" class="form-control" ng-model="category" value="${category}"/>
			<p>{{"残り" + (10 - category.length) + "文字"}}</p>
            本文<br />
            <textarea name="text" cols="100" rows="5" class="form-control" ng-model="value"></textarea>
            <p>{{"残り" + (1000 - value.length) + "文字"}}</p>
            <br />
            <input type="submit" class="btn btn-primary" value="投稿">
        </form>
</div>
 		</div>
    </body>
</html>
