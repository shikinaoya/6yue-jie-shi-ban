<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- jQuery読み込み -->
		<script type="text/javascript" src="./js/jquery.min.js"></script>

		<!-- BootstrapのCSS読み込み -->
		<link rel="stylesheet" href="./css/bootstrap.min.css" rel="stylesheet">
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<!-- BootstrapのJS読み込み -->
		<script src="./js/bootstrap.min.js"></script>

		<!-- AngularJS読み込み -->
		<script src="./js/angular.min.js"></script>

		<!-- JSファイル読み込み -->
		<script src="./js/showlength.js"></script>

		<title>ホーム</title>
	</head>
	<body>
		<div class="bg-light">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="header">
				<ol class="breadcrumb">
					<li class="active"><a href="home" class="btn btn-link">ホーム</a></li>
					<li><a href="newMessage" class="btn btn-link">新規投稿</a></li>
					<li><a href="management" class="btn btn-link">ユーザー管理</a></li>
					<li><a href="logout" class="btn btn-link">ログアウト</a></li>
				</ol>
				<div class="name">
					<h3>
						こんにちは、
						<c:out value="${loginUser.name}" />
						さん
					</h3>
				</div>
				<div class="btn btn-link" onclick="obj=document.getElementById('open').style; obj.display=(obj.display=='none')?'block':'none';">
					<a style="cursor: pointer;">▼投稿の絞込み クリックで展開</a>
				</div>
				<div id="open" style="display: none; clear: both;">
					<form action="findOfCategory" method="get">
						<label class="title">カテゴリー(部分一致)</label>
						<input name="category" class="form-control" value="${category}">
						<input type="submit" class="btn btn-primary" value="絞込み">
					</form>
					<form action="findOfDate" method="get" class="pull-right">
						<label class="title" for="findDate">投稿日（期間指定）</label>
						<label class="col-md-1 col-md-offset-2">開始</label>
						<input type="date" class="form-control" name="startDate" value="2019-06-14" />
						<label class="col-md-1 col-md-offset-2">終了</label>
						<input type="date" class="form-control" name="endDate" value="2019-06-14" />
						<input type="submit" class="btn btn-primary" value="絞込み">
					</form>
				</div>
			</div>
			<div class="panel panel-default">
				<c:forEach items="${messages}" var="message">
					<hr size="3" color="gray">
					<form action="messageDelete" method="get">
						<div class="panel panel-default">
							<div class="panel-heading">
								件名 ：
								<c:out value="${message.subject}" />
							</div>
							<div class="panel-body">
								本文 ：
								<c:out value="${message.text}" />
							</div>
							<div class="panel-body">
								投稿者 ：
								<c:out value="${message.name}" />
							</div>
							<div class="panel-footer">
								投稿日時：
								<fmt:formatDate value="${message.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<div class="id">
								<input type="hidden" name="id" value="${message.id}" />
							</div>
							<c:if test="${loginUser.loginId == message.loginId}">
								<input type="submit" class="btn btn-danger btn-sm" value="削除" />
							</c:if>
						</div>
						<hr size="3" color="gray">
					</form>

					<c:forEach items="${comments}" var="comment">
						<c:if test="${comment.messageId == message.id}">
							<form action="commentDelete" method="get">
								<div class="comment">
									<div class="name">
										投稿者：
										<c:out value="${comment.name}" />
									</div>
									<div class="text">
										<c:out value="${comment.text}" />
									</div>
									<div class="createdDate">
										<fmt:formatDate value="${comment.createdDate}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<div class="id">
										<input type="hidden" name="id" value="${comment.id}" />
									</div>
									<c:if test="${loginUser.loginId == comment.loginId}">
										<input type="submit" class="btn btn-danger btn-sm" value="削除" />
									</c:if>
								</div>
							</form>
							<hr>
						</c:if>
					</c:forEach>

					<form action="comment" method="post">
						<div class="form-group">
							コメント<br />
							<textarea class="form-control" name="text" cols="100" rows="5"
								class="commentbox" ng-model="value" ></textarea>
								<p>{{"残り" + (500 - value.length) + "文字"}}</p>
							<br />
							<div class="id">
								<input type="hidden" name="messageId" value="${message.id}" />
							</div>
							<input type="submit" class="btn btn-primary btn-sm" value="投稿">
						</div>
					</form>
				</c:forEach>
			</div>
		</div>
	</body>
</html>
