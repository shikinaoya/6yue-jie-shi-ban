<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- BootstrapのCSS読み込み -->
		<link rel="stylesheet" href="./css/bootstrap.min.css" rel="stylesheet">
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<!-- jQuery読み込み -->
		<script type="text/javascript" src="./js/jquery.min.js"></script>

		<!-- BootstrapのJS読み込み -->
		<script src="./js/bootstrap.min.js"></script>

		<title>ユーザー管理</title>
	</head>
	<body>
		<div class="bg-light">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="header">
				<ol class="breadcrumb">
					<li class="active"><a href="home" class="btn btn-link">ホーム</a></li>
					<li><a href="newMessage" class="btn btn-link">新規投稿</a></li>
					<li><a href="management" class="btn btn-link">ユーザー管理</a></li>
					<li><a href="logout" class="btn btn-link">ログアウト</a></li>
				</ol>
				<div class="name">
					<h3>ユーザー管理画面</h3>
				</div>
				<a href="signup" class="btn btn-primary">新規登録</a>
			</div>
			<div class="panel panel-default">
				<table class="table table-hover">
				    <thead>
				        <tr>
				            <th>ID</th>
				            <th>Name</th>
				            <th>Office</th>
				            <th>Position</th>
				            <th>Status</th>
				        </tr>
				    </thead>
				    <tbody>
    					<c:forEach items="${users}" var="user">
					        <tr>
					            <th>
					            	<form action="editUser" method="get">
						            	<input type="hidden" name="id" value="${user.id}" />
						            	<input type="hidden" name="loginId" value="${user.loginId}" />
						            	<input type="hidden" name="name" value="${user.name}" />
						            	<input type="hidden" name="officeId" value="${user.officeId}" />
						            	<input type="hidden" name="positionId" value="${user.positionId}" />
						            	<input type="hidden" name="status" value="${user.status}" />
						            	<input type="submit" class="btn btn-link" value="${user.loginId}" />
						            </form>
					            </th>
					            <td><c:out value="${user.name}" /></td>
					            <td><c:out value="${user.office}" /></td>
					            <td><c:out value="${user.position}" /></td>
					            <c:if test="${user.status == 1}">
					            	<td>active</td>
					            </c:if>
					            <c:if test="${user.status == 0}">
					            	<td>inactive</td>
					            </c:if>
					        </tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>
